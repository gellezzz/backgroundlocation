### BackgroundLocation app ###

### What is this repository for? ###

* Getting Location Updates for iOS 8 and above when the App is Killed/Terminated/Suspended
* Version: 1

### Steps to Test ###

1. Download the project.
2. Connect your iOS device with your mac.
3. Launch the app into your iOS device.
4. Kill the app. (Double tap and remove the app from the App Preview)
5. Travel around.

### Who do I talk to? ###

* Angel Ignatov
* [ Aggressione Team ](http://www.aggressione.com/)