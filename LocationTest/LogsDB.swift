//
//  LogsDB.swift
//  LocationTest
//
//  Created by Angel Ignatov on 10/27/15.
//  Copyright © 2015 Aggressione. All rights reserved.
//

import CoreData

@objc(LogsDB)
class LogsDB: NSManagedObject {
    @NSManaged var log: String
    @NSManaged var date: String    
    
    static var ENTITY_NAME: String = "LogsDB"
    
    class func save(log: String) {
        
        let db = CoreDataHelper.getContext(LogsDB.ENTITY_NAME, inBackground: false)
        
        let logsDB: LogsDB = LogsDB(entity: db.entity, insertIntoManagedObjectContext: db.context)
        
        let currentDateTime = NSDate()
        let formatter = NSDateFormatter()
        formatter.timeStyle = NSDateFormatterStyle.MediumStyle
        formatter.dateStyle = NSDateFormatterStyle.MediumStyle
        
        logsDB.date = formatter.stringFromDate(currentDateTime)
        logsDB.log = log
        db.cdh.saveContext(db.context)
    }
    
    class func getAllData() -> [LogsDB] {
        
        let db = CoreDataHelper.getContext(LogsDB.ENTITY_NAME, inBackground: false)
        
        let request = NSFetchRequest(entityName: LogsDB.ENTITY_NAME)
        request.returnsObjectsAsFaults = false
        request.sortDescriptors = [NSSortDescriptor(key: "date", ascending: false)]
        
        var results: [LogsDB] = [LogsDB]()
        
        do {
            try results = db.context.executeFetchRequest(request) as! [LogsDB]
        } catch let error as NSError {
            print(error.localizedDescription)
        }
        
        return results
    }

}