//
//  LocationManagerBackground.swift
//  LocationTest
//
//  Created by Angel Ignatov on 10/27/15.
//  Copyright © 2015 Aggressione. All rights reserved.
//

import Foundation
import CoreLocation
import UIKit

class LocationManagerBackground: NSObject, CLLocationManagerDelegate {
    
    var anotherLocationManager: CLLocationManager!
    
    class var IS_IOS_OR_LATER: Bool {
        
        let Device = UIDevice.currentDevice()
        let iosVersion = NSString(string: Device.systemVersion).doubleValue
        return iosVersion >= 8
    }
    
    class var sharedManager : LocationManagerBackground {
        struct Static {
            static let instance : LocationManagerBackground = LocationManagerBackground()
        }
        return Static.instance
    }
    
    private override init(){
        super.init()
        
    }
    
    func startMonitoringLocation() {
        if (anotherLocationManager != nil) {
            anotherLocationManager.stopMonitoringSignificantLocationChanges()
        }
        
        self.anotherLocationManager = CLLocationManager()
        anotherLocationManager.delegate = self
        anotherLocationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
        anotherLocationManager.activityType = CLActivityType.OtherNavigation
        
        if (LocationManagerBackground.IS_IOS_OR_LATER) {
            anotherLocationManager.requestAlwaysAuthorization()
        }
        anotherLocationManager.startMonitoringSignificantLocationChanges()
    }
    
    func restartMonitoringLocation() {
        anotherLocationManager.stopMonitoringSignificantLocationChanges()
        
        if (LocationManagerBackground.IS_IOS_OR_LATER) {
         anotherLocationManager.requestAlwaysAuthorization()
        }
        
        anotherLocationManager.startMonitoringSignificantLocationChanges()
    }
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let arrayOfLocation = locations as NSArray
        let location = arrayOfLocation.lastObject as! CLLocation
        let coordLatLon = location.coordinate
        
        let latitude: Double  = coordLatLon.latitude
        let longitude: Double = coordLatLon.longitude
        
        LocationsDB.save("no address", latitude: latitude, longitude: longitude)

        
    }
    
}


