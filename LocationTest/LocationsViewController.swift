//
//  ViewController.swift
//  LocationTest
//
//  Created by Angel Ignatov on 10/26/15.
//  Copyright © 2015 Aggressione. All rights reserved.
//

import UIKit
import CoreData
import MapKit

class LocationsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, CLLocationManagerDelegate {

    @IBOutlet var tableView: UITableView!
    
    //var locationManager: LocationManager!
    var coreLocationManager: CLLocationManager = CLLocationManager()
    
    var data: [LocationsDB] = [LocationsDB]()
    
    @IBAction func get(sender: AnyObject) {
        refreshData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        refreshData()
    }

    
    func refreshData() {
        
        data = LocationsDB.getAllData()
        tableView.reloadSections(NSIndexSet(index: 0), withRowAnimation: UITableViewRowAnimation.Fade)

    }
    
    /* tableView */
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
               
        let cell = UITableViewCell(style: UITableViewCellStyle.Subtitle, reuseIdentifier: "Cell")
        
        let row: LocationsDB = data[indexPath.row]
        
        cell.textLabel?.numberOfLines = 0
        cell.textLabel?.text = nil
        
        cell.detailTextLabel?.numberOfLines = 0
        cell.detailTextLabel?.text = "date: \(row.date) \nlatitude: \(row.latitude) \nlongitude: \(row.longitude)"
        
        return cell
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

