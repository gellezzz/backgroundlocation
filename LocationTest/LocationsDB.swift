//
//  Locations.swift
//  LocationTest
//
//  Created by Angel Ignatov on 10/26/15.
//  Copyright © 2015 Aggressione. All rights reserved.
//

import CoreData

@objc(LocationsDB)
class LocationsDB: NSManagedObject {
    @NSManaged var address: String
    @NSManaged var latitude: Double
    @NSManaged var longitude: Double
    @NSManaged var date: String    
    
    static var ENTITY_NAME: String = "LocationsDB" 
    
    class func save(address: String, latitude: Double, longitude: Double) {
        
        let db = CoreDataHelper.getContext(LocationsDB.ENTITY_NAME, inBackground: false)
        
        let locationsDB: LocationsDB = LocationsDB(entity: db.entity, insertIntoManagedObjectContext: db.context)
        
        let currentDateTime = NSDate()
        let formatter = NSDateFormatter()
        formatter.timeStyle = NSDateFormatterStyle.MediumStyle
        formatter.dateStyle = NSDateFormatterStyle.MediumStyle
        
        locationsDB.date = formatter.stringFromDate(currentDateTime)
        locationsDB.address = address
        locationsDB.latitude = latitude
        locationsDB.longitude = longitude
        db.cdh.saveContext(db.context)
        
        let log: String = "SAVE LOCATION: address: \(address) | latitude:\(latitude) | longitude:\(longitude)"        
        LogsDB.save(log)        
    }
    
    class func getAllData() -> [LocationsDB] {
        
        let db = CoreDataHelper.getContext(LocationsDB.ENTITY_NAME, inBackground: false)
        
        let request = NSFetchRequest(entityName: LocationsDB.ENTITY_NAME)
        request.returnsObjectsAsFaults = false
        request.sortDescriptors = [NSSortDescriptor(key: "date", ascending: false)]
        
        var results: [LocationsDB] = [LocationsDB]()
        
        do {
            try results = db.context.executeFetchRequest(request) as! [LocationsDB]
        } catch let error as NSError {
            print(error.localizedDescription)
        }

        return results
    }
}