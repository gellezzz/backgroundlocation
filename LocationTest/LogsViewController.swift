//
//  LogsViewController.swift
//  LocationTest
//
//  Created by Angel Ignatov on 10/27/15.
//  Copyright © 2015 Aggressione. All rights reserved.
//

import UIKit

class LogsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var data: [LogsDB] = [LogsDB]()
    
    @IBOutlet var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        refreshData()
    }
    
    func refreshData() {
        data = LogsDB.getAllData()
        tableView.reloadSections(NSIndexSet(index: 0), withRowAnimation: UITableViewRowAnimation.Fade)
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
                
        let cell = UITableViewCell(style: UITableViewCellStyle.Subtitle, reuseIdentifier: "Cell")
        
        let row: LogsDB = data[indexPath.row]
        
        cell.textLabel?.numberOfLines = 0
        cell.textLabel?.text = nil
        
        cell.detailTextLabel?.numberOfLines = 0
        cell.detailTextLabel?.text = "date: \(row.date) \n\(row.log)"
        
        return cell
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
